import 'package:flutter/material.dart';
import 'package:zog_project_test/utils/constant.dart';

class CheckBox extends StatefulWidget {
  CheckBox({Key key}) : super(key: key);

  @override
  _CheckBoxState createState() => _CheckBoxState();
}

class _CheckBoxState extends State<CheckBox> {
  bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 24.0,
      width: 24.0,
      child: Checkbox(
        key: Key("compCheckbox"),
        checkColor: Colors.white,
        activeColor: cPrimary,
        value: isChecked,
        onChanged: (bool value) {
          setState(
            () {
              isChecked = value;
            },
          );
        },
      ),
    );
  }
}
