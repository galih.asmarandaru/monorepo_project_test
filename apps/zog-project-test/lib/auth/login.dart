import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:zog_project_test/components/button.dart';
import 'package:zog_project_test/components/checkbox.dart';
import 'package:zog_project_test/components/text_field.dart';
import 'package:zog_project_test/utils/constant.dart';

// ignore: must_be_immutable
class LoginPage extends StatefulWidget {
  double loginYOfset;

  LoginPage({Key key, this.loginYOfset}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // Initially password is obscure
  bool _obscureText = true;

  String _password;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        setState(() {
          FocusScope.of(context).unfocus();
        });
      },
      child: AnimatedContainer(
        height: size.height,
        duration: Duration(
          milliseconds: 1000,
        ),
        curve: Curves.fastLinearToSlowEaseIn,
        transform: Matrix4.translationValues(0, widget.loginYOfset, 1),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(35),
              topRight: Radius.circular(35),
            )),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  top: 20,
                  left: 50,
                  right: 50,
                ),
                child: Center(
                  child: Image.asset("assets/img/bg-landing.png"),
                ),
              ),
              Column(
                children: <Widget>[
                  Container(
                      width: size.width,
                      height: size.height * .11,
                      alignment: Alignment.centerLeft,
                      child: Center(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Username",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              TextFieldCustom(
                                width: 0.88,
                                hint: "username",
                              ),
                            ]),
                      )),
                  Container(
                      width: size.width,
                      child: Center(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Password",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              TextFieldCustom(
                                width: 0.88,
                                obscureText: _obscureText,
                                hint: "at least 8 characters",
                                suffixIcon: IconButton(
                                  icon: _obscureText == true
                                      ? const Icon(
                                          Icons.remove_red_eye_outlined,
                                          color: Colors.grey,
                                        )
                                      : const Icon(
                                          Icons.remove_red_eye,
                                          color: Colors.grey,
                                        ),
                                  tooltip: 'Increase volume by 10',
                                  onPressed: () {
                                    setState(() {
                                      _toggle();
                                    });
                                  },
                                ),
                              ),
                            ]),
                      )),
                  Container(
                      width: size.width * .88,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              CheckBox(),
                              Padding(
                                padding: const EdgeInsets.only(
                                  left: 5,
                                ),
                                child: Text(
                                  "remember me",
                                ),
                              ),
                            ],
                          ),
                          Text.rich(
                            TextSpan(children: <TextSpan>[
                              TextSpan(
                                text: "Forgot Password?",
                                style: TextStyle(
                                  color: cPrimary,
                                ),
                                recognizer: new TapGestureRecognizer()
                                  ..onTap = () => debugPrint('Tap Here onTap'),
                              ),
                            ]),
                          ),
                        ],
                      )),
                  Container(
                    margin: EdgeInsets.only(
                      top: 30,
                    ),
                    child: ButtonRounded(
                      width: .88,
                      text: Text(
                        "Login",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 100,
                      left: 100,
                      right: 100,
                      bottom: 0,
                    ),
                    padding: EdgeInsets.only(
                      bottom: 160,
                    ),
                    child: Text.rich(
                      TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                            text: "Don't have an account ? ",
                          ),
                          TextSpan(
                            text: "Sign Up",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: cPrimary,
                            ),
                            recognizer: TapGestureRecognizer()..onTap = () {},
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
