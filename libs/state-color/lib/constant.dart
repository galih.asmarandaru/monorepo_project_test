import 'package:flutter/material.dart';

const cPrimary = Color(0xFF5387FE);
const cSecondary = Color(0xFF212121);
const lightGray = Color(0xFFEEEEEE);
